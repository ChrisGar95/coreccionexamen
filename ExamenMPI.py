import random

import time
import multiprocessing
from mpi4py import MPI 

 

 

def how_many_max_values_sequential(ar):
    #find max value of the list

    maxValue = 0

    for i in range(len(ar)):

        if i == 0:

            maxValue = ar[i]

        else:

            if ar[i] > maxValue:

                maxValue = ar[i]
               
    #find how many max values are in the list

    contValue = 0

    for i in range(len(ar)):

        if ar[i] == maxValue:

            contValue += 1
    return contValue

 

# Complete the how_many_max_values_parallel function below.
def how_many_max_values_parallelMpi(ar):
   
   resultado=0
   suma3=0
   tam= 5000000
   
   comm = MPI.COMM_WORLD 
   numtasks = comm.size 
   rank = comm.Get_rank()

   

   if rank==0:
       data1= 0
       data2= 8000000
       destination_process = 5
       comm.send(data1,dest=destination_process)
       comm.send(data2,dest=destination_process)
       
 
   if rank==1:
       data1= 8000000
       data2= 16000000
       destination_process = 6
       comm.send(data1,dest=destination_process)
       comm.send(data2,dest=destination_process)
       

   if rank==2:
       data1= 16000000
       data2= 24000000
       destination_process = 7
       comm.send(data1,dest=destination_process)
       comm.send(data2,dest=destination_process)
       

   if rank==3:
       data1= 24000000
       data2= 32000000
       destination_process = 8
       comm.send(data1,dest=destination_process)
       comm.send(data2,dest=destination_process)
       

   if rank==4:
       data1= 32000000
       data2= 40000000
       destination_process = 9
       comm.send(data1,dest=destination_process)
       comm.send(data2,dest=destination_process)
       
	   

   if rank==5:
       data1=comm.recv(source=0)
       data2=comm.recv(source=0)
       
       resultado=how_many_max_values_sequential(ar[data1:data2])
       destination_process = 6
       comm.send(resultado,dest=destination_process)
	    
	    
   if rank==6:
       data1=comm.recv(source=1)
       data2=comm.recv(source=1)
       resultado=comm.recv(source=5)
       suma=resultado

       resultado=how_many_max_values_sequential(ar[data1:data2])
       suma=suma+resultado
       destination_process = 7
       comm.send(suma,dest=destination_process)


   if rank==7:
       data1=comm.recv(source=2)
       data2=comm.recv(source=2)
       
       suma=comm.recv(source=6)
       suma1=suma

       resultado=how_many_max_values_sequential(ar[data1:data2])
       suma1=suma1+resultado
       destination_process = 8
       comm.send(suma1,dest=destination_process)
       
   if rank==8:
       data1=comm.recv(source=3)
       data2=comm.recv(source=3)
       
       suma1=comm.recv(source=7)
       suma2=suma1

       resultado=how_many_max_values_sequential(ar[data1:data2])
       suma2=suma2+resultado
       destination_process = 9
       comm.send(suma2,dest=destination_process)
      
   if rank==9:
       data1=comm.recv(source=4)
       data2=comm.recv(source=4)
       
       suma2=comm.recv(source=8)
       suma3=suma2

       resultado=how_many_max_values_sequential(ar[data1:data2])
       suma3=suma3+resultado
       print("resulPar")
       print(suma3)
   
   return suma3
    
    #implement your solution

 

if __name__ == '__main__':
    
    ar_count = 40000000

    #Generate ar_count random numbers between 1 and 30

    ar = [random.randrange(1,30) for i in range(ar_count)]
    
    inicioSec = time.time()

    resultSec = how_many_max_values_sequential(ar)
    print(resultSec)

    finSec =  time.time()

   

    inicioPar = time.time()  
 
    resultPar = how_many_max_values_parallelMpi(ar)

    finPar = time.time() 

    #print('Results are correct!\n' if resultSec == resultPar else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms with %d items\n' % ((finSec - inicioSec)*1000, ar_count))

    print('Parallel Process took %.3f ms with %d items\n' % ((finPar - inicioPar)*1000, ar_count))





