import random

import time
import multiprocessing
from mpi4py import MPI 

 

 

def how_many_max_values_sequential(ar):
    #find max value of the list

    maxValue = 0

    for i in range(len(ar)):

        if i == 0:

            maxValue = ar[i]

        else:

            if ar[i] > maxValue:

                maxValue = ar[i]
               
    #find how many max values are in the list

    contValue = 0

    for i in range(len(ar)):

        if ar[i] == maxValue:

            contValue += 1
    return contValue

 

# Complete the how_many_max_values_parallel function below.

def how_many_max_values_parallelProceso(ar):
   resultado=0
   veces=0
   
   numDatos=[ar[0:4000000],ar[4000000:8000000],ar[8000000:12000000],ar[12000000:16000000],ar[16000000:20000000],ar[20000000:24000000],ar[24000000:28000000],ar[28000000:32000000],ar[32000000:36000000],ar[36000000:40000000]]
   numProcesos=10
   pool = multiprocessing.Pool(numProcesos)
   resultado=pool.map(how_many_max_values_sequential,numDatos)
   pool.close()
   pool.join()

   for i in resultado:
       veces=veces+i
   
   return veces


    
    #implement your solution

 

if __name__ == '__main__':
    
    ar_count = 40000000

    #Generate ar_count random numbers between 1 and 30

    ar = [random.randrange(1,30) for i in range(ar_count)]
    
    

    inicioSec = time.time()

    #resultSec = how_many_max_values_sequential(ar)

    finSec =  time.time()

   

    inicioPar = time.time()   

    resultPar = how_many_max_values_parallelProceso(ar)
    

    finPar = time.time() 

   

    print('Results are correct!\n' if resultSec == resultPar else 'Results are incorrect!\n')

    print('Sequential Process took %.3f ms with %d items\n' % ((finSec - inicioSec)*1000, ar_count))

    print('Parallel Process took %.3f ms with %d items\n' % ((finPar - inicioPar)*1000, ar_count))





